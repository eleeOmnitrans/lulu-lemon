﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;
using Renci.SshNet;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace ShipmentJsonSplitter
{
    class Program
    {
        static void Main(string[] args)
        {
            //string jsonfolder = ConfigurationManager.AppSettings["IncomingJsonFolder"];
            string jsonoutputfolder = ConfigurationManager.AppSettings["OutgoingJsonFolder"];
            //string jsonArchivefolder = ConfigurationManager.AppSettings["IncomingJsonArchiveFolder"];
            //string jsonoutputArchivefolder = ConfigurationManager.AppSettings["OutgoingJsonArchiveFolder"];
            int NumberOfRecordsMax = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfRecordsMax"]);
            int NumberOfCharachterMax = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfCharachterMax"]);

            int iCount = 0;
            int iCount2 = 0;
            int splitFileNumber;

            String Host = "MTLvCom17";
            int Port = 22;
            String RemoteFileName = "TheDataFile.txt";
            String LocalDestinationFilename = "TheDataFile.txt";
            String Username = "MITSys_DellBoomi";
            String Password = "rsgHaD4gDbRAWdoEf5IE";
            string sftpfolder = "/Clients/Lululemon/40_Converted";

            string outputFilename = "", outputFilenameFullpath = "";
            List<string> listOutputFilename = new List<string>();


            SqlConnection sqlcnn = new SqlConnection();
            sqlcnn.ConnectionString = ConfigurationManager.ConnectionStrings["AzureAPP"].ConnectionString;
            sqlcnn.Open();
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlcnn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "LULU.[BatchIdNewGet]";
            SqlParameter oParam = sqlCmd.Parameters.Add("@BatchId", SqlDbType.Int);
            sqlCmd.Parameters["@BatchId"].Direction = ParameterDirection.Output;
            sqlCmd.ExecuteNonQuery();
            int BatchId = (int)(sqlCmd.Parameters["@BatchId"].Value);

            try
            {


                ///
                #region 1. Delete all files before processing
                DirectoryInfo d = new DirectoryInfo(jsonoutputfolder);
                foreach (FileInfo fi in d.GetFiles())
                {
                    fi.Delete();
                }
                #endregion

                #region 2. Sftp download and log json files

                using (var sftp = new SftpClient(Host, Port, Username, Password))
                {
                    sftp.Connect();

                    sftp.ChangeDirectory(sftpfolder);
                    var files = sftp.ListDirectory(sftpfolder);

                    sqlCmd.CommandText = "LULU.JsonFileInsert";

                    foreach (var file in files)
                    {
                        DateTime fileCreatedDT = file.LastWriteTime;
                        string filename = file.Name;
                        string remotefilenamefullpath = file.FullName;
                        int fileSizeinKB = Convert.ToInt32(file.Length / 1024);

                        if (filename.IndexOf(".json") > -1)
                        {

                            outputFilename = jsonoutputfolder + "\\" + filename;
                            sqlCmd.Parameters.Clear();
                            SqlParameter param = sqlCmd.Parameters.AddWithValue("@BatchId", BatchId);
                            param = sqlCmd.Parameters.AddWithValue("@Filename", filename);
                            param = sqlCmd.Parameters.AddWithValue("@FilenameFullpath", outputFilename);
                            param = sqlCmd.Parameters.AddWithValue("@LastWriteTime", fileCreatedDT);
                            param = sqlCmd.Parameters.AddWithValue("@FilesizeInKb", fileSizeinKB);
                            param = sqlCmd.Parameters.Add("@FileExist", SqlDbType.Bit);
                            sqlCmd.Parameters["@FileExist"].Direction = ParameterDirection.Output;
                            sqlCmd.ExecuteNonQuery();

                            bool fileExisted = (bool)(sqlCmd.Parameters["@FileExist"].Value);
                            if (!fileExisted)
                            {
                                //json file download
                                Console.WriteLine(filename);


                                Stream file1 = File.OpenWrite(outputFilename);
                                sftp.DownloadFile(remotefilenamefullpath, file1);
                                file1.Close();
                            }
                            else
                            {

                            }


                        }
                    }




                    sftp.Disconnect();
                    sftp.Dispose();
                }

                #endregion
                ///


                foreach (FileInfo fi in d.GetFiles())
                {
                    if (fi.Extension == ".json" && fi.Length > NumberOfCharachterMax)
                    {
                        listOutputFilename.Clear();

                        splitFileNumber = 1;

                        #region Read json file in memory stream
                        StreamReader sr = new StreamReader(fi.FullName);

                        JsonTextReader jtr = new JsonTextReader(sr);
                        JsonSerializer js = new JsonSerializer();
                        StringBuilder sb = new StringBuilder();
                        sb.Clear();

                        while (jtr.Read())
                        {
                            if (jtr.TokenType == JsonToken.StartArray)
                            {
                                string jsonArrayName = jtr.Path;
                                iCount = 0;
                                iCount2 = 0;


                                Console.WriteLine(jsonArrayName);
                                while (jtr.Read())
                                {
                                    if (jtr.TokenType == JsonToken.EndArray)
                                    {
                                        //Console.WriteLine(iCount);

                                        //dump to seperate file
                                        //StreamWriter sw = new StreamWriter(jsonoutputfolder + "\\" + fi.Name.Replace(".json", "") + "_" + jsonArrayName + ".json");
                                        //StreamWriter sw = new StreamWriter(jsonoutputfolder + "\\" + jsonArrayName + "_" + fi.Name.Replace(".json", "").Replace("SBShipment_", "") + ".json");
                                        //sw.Write("[" + "\n" + sb.ToString() + "\n" + "]");
                                        //sw.Close();
                                        //sb.Clear();
                                        break;
                                    }
                                    if (jtr.TokenType == JsonToken.StartObject)
                                    {
                                        var json = js.Deserialize(jtr);

                                        if (sb.Length + json.ToString().Length > NumberOfCharachterMax)
                                        {
                                            outputFilename = fi.Name.Replace(".json", "") + "_" + splitFileNumber.ToString() + ".json";
                                            outputFilenameFullpath = jsonoutputfolder + "\\" + outputFilename;
                                            StreamWriter sw = new StreamWriter(outputFilenameFullpath);
                                            listOutputFilename.Add(outputFilenameFullpath);
                                            sw.Write("[" + "\n" + sb.ToString() + "\n" + "]");
                                            sw.Close();
                                            sb.Clear();
                                            splitFileNumber++;
                                            iCount = 0;
                                        }

                                        if (iCount == 0)
                                        {
                                            sb.Append(json);
                                        }
                                        else
                                        {
                                            sb.Append(",\n" + json);
                                        }

                                        iCount++;
                                        iCount2++;



                                        //if (iCount >= NumberOfRecordsMax)
                                        //{
                                        //    outputFilename = fi.Name.Replace(".json", "") + "_" + splitFileNumber.ToString() + ".json";
                                        //    outputFilenameFullpath = jsonoutputfolder + "\\" + outputFilename;
                                        //    StreamWriter sw = new StreamWriter(outputFilenameFullpath);
                                        //    listOutputFilename.Add(outputFilenameFullpath);
                                        //    sw.Write("[" + "\n" + sb.ToString() + "\n" + "]");
                                        //    sw.Close();
                                        //    sb.Clear();



                                        //    splitFileNumber++;
                                        //    iCount = 0;
                                        //}


                                    }
                                }

                            }
                        }

                        sr.Close();
                        jtr.Close();

                        #endregion

                        //Console.WriteLine(iCount.ToString());

                        outputFilename = fi.Name.Replace(".json", "") + "_" + splitFileNumber.ToString() + ".json";
                        outputFilenameFullpath = jsonoutputfolder + "\\" + outputFilename;
                        StreamWriter sw2 = new StreamWriter(outputFilenameFullpath);
                        listOutputFilename.Add(outputFilenameFullpath);
                        sw2.Write("[" + "\n" + sb.ToString() + "\n" + "]");
                        sw2.Close();
                        sb.Clear();
                        splitFileNumber++;
                        iCount = 0;

                        if (iCount2 < NumberOfRecordsMax)
                        {
                            //delete all output split files.
                            //foreach (string filename in listOutputFilename)
                            //{
                            //    File.Delete(filename);
                            //}

                            //delete all output split

                        }
                        else
                        {
                            //uncomment
                            //#region upload and log split json files
                            //string emailbody = "";
                            //var sftp = new SftpClient(Host, Port, Username, Password);
                            //sftp.Connect();

                            //sftp.ChangeDirectory(sftpfolder);
                            //var files = sftp.ListDirectory(sftpfolder);

                            ////upload split json files
                            //foreach (string filename in listOutputFilename)
                            //{
                            //    var fileStream = new FileStream(filename, FileMode.Open);
                            //    if (fileStream != null)
                            //    {
                            //        sftp.UploadFile(fileStream, sftpfolder + "/" + Path.GetFileName(filename));

                            //        //log in database
                            //        sqlCmd.CommandText = "LULU.JsonSplitFileInsert";
                            //        sqlCmd.Parameters.Clear();
                            //        SqlParameter param = sqlCmd.Parameters.AddWithValue("@BatchId", BatchId);
                            //        param = sqlCmd.Parameters.AddWithValue("@JsonFilename", fi.Name);
                            //        param = sqlCmd.Parameters.AddWithValue("@JsonSplitFilename", Path.GetFileName(filename));
                            //        sqlCmd.ExecuteNonQuery();
                            //    }

                            //    emailbody += Path.GetFileName(filename) + "\n";
                            //}
                            //sftp.Disconnect();
                            //sftp.Dispose();

                            //emailbody = fi.Name + " has " + iCount2.ToString() + " items, exceeding 9999 items limit \n\n" + fi.Name + " is split into " + listOutputFilename.Count.ToString() + 
                            //    " files:\n\n" + emailbody;
                            
                            //SendEmailNotification(emailbody);

                            //#endregion
                        }
                    }
                }


                sqlcnn.Close();
                sqlcnn.Dispose();

            }
            catch (Exception ex)
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("alert@omnitrans.com");
                message.To.Add(new MailAddress(ConfigurationManager.AppSettings["AlertEmailAddress"]));
                message.Subject = "ShipmentJsonSplitter execution error";
                message.IsBodyHtml = false; //to make message body as html  
                message.Body = ex.Message;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("ericlee.omnitrans@gmail.com", "mail2Omnitrans!");
                smtp.Credentials = new NetworkCredential("ericlee.omnitrans@gmail.com", "itjesfmlqxpcknhm");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);

            }
        }

        

        public static void SendEmailNotification(string msg)
        {
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress("alert@omnitrans.com");
            string fromAddress = ConfigurationManager.AppSettings["EmailNotification"];
            string[] arrFromAddress = fromAddress.Split(';');

            foreach (string emailAddr in arrFromAddress)
            {
                message.To.Add(new MailAddress(emailAddr));
            }

            
            message.Subject = "LuLu Lemon Split Json file";
            message.IsBodyHtml = false; //to make message body as html  
            message.Body = msg;
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; //for gmail host  
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("ericlee.omnitrans@gmail.com", "mail2Omnitrans!");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);
        }

    }


}
